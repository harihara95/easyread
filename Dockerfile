FROM openjdk:8-alpine
ADD /build/libs/gs-rest-service-0.1.0.jar easyread.jar
ENTRYPOINT ["java","-jar","/easyread.jar"]
