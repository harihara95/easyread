package com.bootcamp.sample;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShop {

    @RequestMapping("/application")
    public ApplicationName getApplicationName()
    {
        return new ApplicationName("Book Shop");
    }

}
