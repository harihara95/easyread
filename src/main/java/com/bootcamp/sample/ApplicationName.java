package com.bootcamp.sample;

public class ApplicationName {
    private String name;

    public ApplicationName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
